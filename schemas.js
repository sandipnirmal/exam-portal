var OptionsSchema = new Schema({
    options: String,
    expression: String
});

var QuestionSchema = new Schema({
    question: String,
    options: {
        A: String,
        B: String
        C: String
        D: String
        E: String
    },
    answers: String
});

var CommentScema = new Schema({
    username: Srting,
    comment: String
});

var TagSchema = new Schema({
    tag: String
});

var ExamSchema = new Schema({
    title: String,
    questions: [Question.schema],
    author: String,
    comments: [Comment.schema],
    category: string,
    tags: [Tag.schema]
});
