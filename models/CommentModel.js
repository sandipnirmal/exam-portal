/**
 * Comment model schema
 * @description defines Comment model
 */
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

/**
 *	Schema Defination
 */
var CommentSchema = new Schema({
    text: String,
    author: String
});
mongoose.model('Comment', CommentSchema);
