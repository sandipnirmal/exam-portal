/**
 *	Exam model schema
 * @description defines exam model
 */
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var Comment = require("./QuestionModel");
var Comment = require("./CommentModel");
var Comment = require("./TagModel");

/**
 *	Schema defination
 */
var ExamSchema = new Schema({
    title: String,
    questions: [Question.schema],
    author: String,
    comments: [Comment.schema],
    category: string,
    tags: [Tag.schema]
});
mongoose.model('Exam', ExamSchema);
