/**
 * Comment model schema
 * @description defines Comment model
 */
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

/**
 *	Schema Defination
 */
var QuestionSchema = new Schema({
    question: String,
    options: {
        A: String,
        B: String
        C: String
        D: String
        E: String
    },
    answers: String
});
mongoose.model('Question', QuestionSchema);
