/**
 * Tag model schema
 * @description defines Tag model
 */
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

/**
 *	Schema Defination
 */
var TagSchema = new Schema({
    tag: String
});
mongoose.model('Tag', TagSchema);
