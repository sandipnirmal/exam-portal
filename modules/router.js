/**
 *
 */
var restify = require('restify'),
    fs = require('fs');

var controllers = {},
    controllers_path = process.cwd() + '/controllers';

// readfiles for controllers and load them
fs.readdirSync(controllers_path).forEach(function(file) {
    if (file.indexOf('.js') != -1) {
        controllers[file.split('.')[0]] = require(controllers_path + '/' + file)
    }
})

// create rest server
var server = restify.createServer();

server
    .use(restify.fullResponse())
    .use(restify.bodyParser())

//Exam start
server.post("exam", exam.createExam);
server.get("exam/:id", exam.retrieveExam);
server.put("exam/:id", exam.updateExam);
server.del("exam/:id", exam.deleteExam);
//End Exam

//Question start
server.post("question", question.createQuestion);
server.get("question", question.retrieveQuestion);
server.put("question", question.updateQuestion);
server.del("question", question.deleteQuestion);
//End Question

var port = process.env.PORT || 3000;
server.listen(port, function(err) {
    if (err)
        console.error(err)
    else
        console.log('App is ready at : ' + port)
})

if (process.env.environment == 'production')
    process.on('uncaughtException', function(err) {
        console.error(JSON.parse(JSON.stringify(err, ['stack', 'message', 'inner'], 2)))
    })
